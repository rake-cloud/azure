<#
 .NOTES
	FileName:		deploy-rds.ps1
	Version:		0.1.2
	Author:			Craig Eversole, RAKE Digital Company
	Create Date:	2017-05-30
	Purpose:		General Deployment Script

 .SYNOPSIS
    Provisions storage on a VM
	Configures a RDS Farm

#>
Param (
    [string]$cbHost,
    [string]$dtCollection,
    [string]$raCollection,
	[string]$raDescription,
	[string]$sessionHosts,
	[string]$certPassword,
	[string]$certStorageAccount,
	[string]$certStorageAccountSAS,
	[string]$certBlob,
    [string]$domainName
)

# Get session host array
[string[]]$hosts = $sessionHosts.Replace("'","").Split(",") 

if (-not (Get-Module -Name RemoteDesktop) ) {
    Import-Module -Name RemoteDesktop
}

if (Get-RDSessionCollection -ConnectionBroker $cbHost | Where-Object {$_.CollectionName -eq $dtCollection }) {
    Remove-RDSessionCollection –CollectionName $dtCollection –ConnectionBroker $cbHost -Force
}

if (-not (Get-RDSessionCollection -ConnectionBroker $cbHost | Where-Object {$_.CollectionName -eq $raCollection })) {
    New-RDSessionCollection -CollectionName $raCollection -SessionHost $hosts -CollectionDescription $raDescription -ConnectionBroker $cbHost
	New-RDRemoteApp -CollectionName $raCollection -ConnectionBroker $cbHost -Alias Notepad -DisplayName Notepad -FilePath "C:\windows\notepad.exe"
}

Set-Location Cert:\LocalMachine\My
if (-not (Get-ChildItem | Where-Object {$_.Subject -Match "CN=\*.$domainName*"})) {

    # Download certificate
    $localTargetDirectory = "C:\WindowsAzure\TempDownload"
    $sasURI = "https://$certStorageAccount.blob.core.windows.net/arm/$certBlob"
    New-Item -ItemType Directory -Path $localTargetDirectory -Force
    Invoke-WebRequest -Uri ($sasURI + $certStorageAccountSAS) -OutFile ($localTargetDirectory + "\" + $certBlob)

    # Certificate Password
    $Password = ConvertTo-SecureString -String $certPassword -AsPlainText -Force

    # Certificate Paty
    $certPath  = ($localTargetDirectory + "\" + $certBlob)
 
    # Configure RD Connection Broker For SSO
    set-RDCertificate -Role RDRedirector -ImportPath $certPath -Password $Password -ConnectionBroker $cbHost -Force
 
    # Configure RD Connection Broker For Publishing
    set-RDCertificate -Role RDPublishing -ImportPath $certPath -Password $Password -ConnectionBroker $cbHost -Force
 
    # Configure RD Connection Broker For WebAccess
    set-RDCertificate -Role RDWebAccess -ImportPath $certPath -Password $Password -ConnectionBroker $cbHost -Force
 
    # Configure RD Connection Broker For Gateway
    set-RDCertificate -Role RDGateway -ImportPath $certPath -Password $Password -ConnectionBroker $cbHost -Force

}