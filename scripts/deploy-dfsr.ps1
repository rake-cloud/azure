<#
 .NOTES
	FileName:		deploy-dfsr.ps1
	Version:		0.1.2
	Author:			Craig Eversole, RAKE Digital Company
	Create Date:	2017-06-13
	Purpose:		General Deployment Script

 .SYNOPSIS
    Provisions storage on a VM
	Deploys domain joined File Server and DFS Roles
	Configures namespace and shares
	Configures dfs replication

#>
Param (
    [string]$domainUser,
	[string]$domainPassword,
	[string]$domainName,
    [string]$fileServer,
	[string]$fileServer1,
	[string]$domainController,
    [string]$dfsName,
	[string]$dfsRoot,
	[uint64]$sizeInGB,
	[string]$shares
)

# Install Windows File Server and DFS Roles
Install-WindowsFeature -Name FS-FileServer,FS-DFS-Namespace,FS-DFS-Replication -IncludeManagementTools

# Get shares array
[string[]]$fileShares = $shares.Replace("'","").Split(",") 

# Create storage pool and shares if the DFS root does not exist
if (-Not (Test-Path F:\dfsroots) ) {
    
    # Get all available disks and create new storage pool
    $NewDisks = Get-PhysicalDisk -CanPool $True -FriendlyName "Msft Virtual Disk"
    New-StoragePool -FriendlyName $dfsName -StorageSubSystemFriendlyName "Windows Storage*" -PhysicalDisks $NewDisks -ResiliencySettingNameDefault Simple

    # Create new volume in storage pool and assign a drive letter
    $volumeSize = $sizeInGB * 1GB
    New-Volume -StoragePoolFriendlyName $dfsName -FriendlyName $dfsName -Size $volumeSize -ResiliencySettingName Simple -ProvisioningType Thin -FileSystem NTFS -AccessPath "F:"

   # Create a series of folders to share
    $folders = @()
    $folders += ("F:\dfsroots\" + $dfsRoot)
    $fileShares | ForEach {$folders +=  ("F:\shares\" + ($_));}
    mkdir -path $folders

    # Create shares for the new folders from the previous step
    $folders | ForEach-Object {$sharename = (Get-Item $_).name; New-SMBShare -Name $shareName -Path $_ -FullAccess Everyone}

    # Assign the shares from the previous step to the namespace
	$domainPass = $domainPassword | ConvertTo-SecureString -Force -AsPlainText
	$domainCred = New-Object System.Management.Automation.PSCredential("$domainName\$domainUser",$domainPass)
    $cimSession = New-CimSession -Credential $domainCred -ComputerName $domainController -Authentication kerberos
    $folders | Where-Object {$_ -like "*shares*"} | ForEach-Object {$name = (Get-Item $_).name; $DfsPath = ('\\' + $domainName + '\' + $dfsRoot + '\' + $name); $targetPath = ('\\' + $fileServer + '\' + $name); New-DfsnFolderTarget -CimSession $cimSession -Path $dfsPath -TargetPath $targetPath;}

	# Install DFSR and setup replication
	$servers = ($fileServer1,$fileServer)
	foreach ($server in $servers) {Install-WindowsFeature -Credential $domainCred FS-DFS-Replication –IncludeManagementTools –ComputerName $server}

	New-DfsReplicationGroup -GroupName RG01
	New-DfsReplicatedFolder -GroupName RG01 -FolderName $fileShares
	Add-DfsrMember -GroupName RG01 -ComputerName $servers
	Add-DfsrConnection -GroupName RG01 -SourceComputerName $fileServer1 -DestinationComputerName $fileServer

	$folders | Where-Object {$_ -like "*shares*"} | ForEach-Object {$name = (Get-Item $_).name; $dfsnPath = ('\\' + $domainname + '\' + $dfsRoot + '\' + $name); $localPath = ('F:\shares\' + $name); Set-DfsrMembership -GroupName RG01 -FolderName $name -ComputerName $fileServer1 -ContentPath $localPath -dfsnPath $dfsnPath -PrimaryMember $true -Force; Set-DfsrMembership -GroupName RG01 -FolderName $name -ComputerName $fileServer -ContentPath $localPath -dfsnPath $dfsnPath -Force;}
	Update-DfsrConfigurationFromAD –ComputerName $servers
}