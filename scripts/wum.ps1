# Allow unrestricted access for this powershell
session
Set-ExecutionPolicy Unrestricted -Force

# Download and Install the Windows Updates PowerShell Module if it doesn't exist

If (-not(Test-Path C:\Windows\System32\WindowsPowerShell\v1.0\Modules\PSWindowsUpdate)) {

$source = "https://gallery.technet.microsoft.com/scriptcenter/2d191bcd-3308-4edd-9de2-88dff796b0bc/file/41459/47/PSWindowsUpdate.zip"
$destination = Join-Path -Path $env:TEMP -ChildPath "PSWindowsUpdate.zip"
$dscHome = "C:\Windows\System32\WindowsPowerShell\v1.0\Modules"
$wc = New-Object system.net.webclient

$wc.downloadFile($source,$destination)

$shell = New-Object -ComObject shell.application

$files = $shell.namespace($destination).items()

$shell.NameSpace($dscHome).copyHere($files)

Remove-Item $destination



}

#Install the Update module if it doesn't exist

if (-not (Get-Module -Name PSWindowsUpdate) ) {

	Import-Module -Name PSWindowsUpdate

}